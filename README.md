# sync-script

## Commands
- `setup` - self install to local bin. Also checks that *'~/bin'* is in PATH environment variable for the symlink to work; ensures that $PATH settings are retained between sessions (*'~/.profile*')
- `config` - configures settings that the sync-script uses when interacting with Server and Rsync; such as server details and exclusion list. This command is *'interactive'*, outputs lines of texts, prompts to enter information
- `test` - test SSH and DB connection with the provided credentials; ensure all passed, else re-run `config`

## Usage
1. Copy (*'sync-script'*) to server `curl -O https://bitbucket.org/pbwebdev/sync-script/raw/HEAD/sync-script`
2. Add execute permission to script `chmod +x sync-script`
3. Run `./sync-script setup` then relogin to server or run `source ~/.profile`
4. CD to the directory you want to work
5. Run `sync-script config` then `sync-script <LOCAL> <REMOTE> [OPTION]`

  - **LOCAL**; path is relative to current working directory
  - `sync-script.cfg` file should also reside in the cwd
  - **REMOTE**; path is relative to remote path set in config
  - **OPTION**; additional options to pass to rsync

## Example
- After following **USAGE** steps 1-3
- CD to /var/www/stage-example.com
- Run `sync-script config` and fill up
- Finally run `sync-script public/ public/`
- or first test using `sync-script public/ public/ --dry-run`

```
### sync-script.cfg ###
###     EXAMPLE     ###

# Local Database
;LDB_USER=staging_ex
;LDB_PASS=$+4g1n6p@$s
;LDB_NAME=staging_usr

# Remote Server
;RSV_HOST=123.456.789
;RSV_USER=ubuntu
;RSV_PATH=/var/www/prod-example.com

# Remote Database
;RDB_USER=prod_ex
;RDB_PASS=pR0dp4s$wd
;RDB_NAME=prod_usr

# Exclude List
.git*
.htaccess
configuration.php
cache
```
